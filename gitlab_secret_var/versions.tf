terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    random = {
      source = "hashicorp/random"
    }
  }
  required_version = ">= 0.13"
}
