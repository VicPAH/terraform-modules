resource "random_password" "pw" {
  count            = var.create ? 1 : 0
  length           = var.length
  special          = true
  override_special = "@:+/="
}
resource "gitlab_project_variable" "pw" {
  count             = var.create ? 1 : 0
  project           = var.project
  key               = var.name
  value             = random_password.pw[0].result
  protected         = true
  masked            = var.variable_type != "file"
  variable_type     = var.variable_type
  environment_scope = var.environment_scope
}
