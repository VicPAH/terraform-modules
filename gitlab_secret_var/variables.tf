variable "create" {
  type    = bool
  default = true
}

variable "project" {
  type = string
}
variable "name" {
  type = string
}
variable "length" {
  type    = number
  default = 64
}
variable "variable_type" {
  type    = string
  default = null
}
variable "environment_scope" {
  type    = string
  default = "*"
}
